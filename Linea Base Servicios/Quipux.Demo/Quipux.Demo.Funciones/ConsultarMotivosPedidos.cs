using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using SDI.API.Servicios.Base.Utilidades;
using SDI.API.Servicios.Base.Utilidades.Funciones;
using SDI.API.Servicios.DTO;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace SDI.MotivosPedido.Funciones
{


    /// <summary>
    /// Funcion-servicio sincronizar desde SAP ERP hacia el Consolidador de Pedidos los Motivos de Pedidos de SD que corresponden a Motivos de Devolución, Motivos de Cambio Mano a Mano, Motivos de Rechazo y Motivos de Reposiciones.
    /// </summary>
    /// <param name="req">Peticion HTTP no requerida</param>
    /// <param name="log"></param>
    /// <returns>Lista de registros vigentes obtenidos</returns>
    public static class ConsultarMotivosPedidos
    {
        #region "Consultar todo"
        [FunctionName("ConsultarMotivosPedido")]
        public static async Task<IActionResult> ConsultarTodo(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "MotivosPedido/Consultar")] HttpRequest req,
            ILogger log)        
        
        {
            try
            {
                MethodBase metodo = MethodBase.GetCurrentMethod();
                Core.CoreMotivosPedido MotivosPedido = new Core.CoreMotivosPedido();
                log.LogInformation("Se ha recibido una peticion en la Azure Function: " + CoreUtilidades.ObtenerMetodoOrigen(metodo));
                List<MotivoPedidoDTO> motivos = MotivosPedido.Consultar();
                return new OkObjectResult(motivos);
            }
            catch (SDIException appEx)
            {
                log.LogError(string.Concat(appEx.Codigo, ": ", (appEx.DescripcionTecnica ?? appEx.Message), " ", appEx.Origen));
                return new BadRequestObjectResult(new FailedResultObject()
                { Codigo = Convert.ToInt32(Errores.Codigo_ConsultaFunction), Descripcion = appEx.Mensaje });

            }
        }
        
        #endregion 

        #region Consultar por ID
        /// <summary>
        /// Funcion-servicio que permite obtener un registro de Motivos pedido segun ID
        /// </summary>
        /// <param name="req">Peticion HTTP con "id=textoGuid"</param>
        /// <param name="log"></param>
        /// <returns>Registro obtenido</returns>
        [FunctionName("ConsultarPorIdMotivosPedido")]

        public static async Task<IActionResult> ConsultarPorId(
        [HttpTrigger(AuthorizationLevel.Function, "get", Route = "MotivosPedido/ConsultarPorId")] HttpRequest req,
        ILogger log)
        {
            try
            {
                MethodBase metodo = MethodBase.GetCurrentMethod();
                MotivoPedidoDTO motivo = new MotivoPedidoDTO();

                Core.CoreMotivosPedido core = new Core.CoreMotivosPedido();
                log.LogInformation("Se ha recibido una peticion en la Azure Function: " + CoreUtilidades.ObtenerMetodoOrigen(metodo));
                string id = req.Query["id"];
                if (string.IsNullOrWhiteSpace(id))
                {
                    throw new SDIException((int)Errores.Codigo_Parametros, metodo);
                }
                motivo = core.ConsultarPorId(id);
                return new OkObjectResult(motivo);
            }
            catch (SDIException appEx)
            {
                log.LogError(string.Concat(appEx.Codigo, ": ", (appEx.DescripcionTecnica ?? appEx.Message), " ", appEx.Origen));
                return new BadRequestObjectResult(new FailedResultObject()
                { Codigo = Convert.ToInt32(Errores.Codigo_ConsultaFunction), Descripcion = appEx.Mensaje });
            }
        }
        #endregion

    }


}

