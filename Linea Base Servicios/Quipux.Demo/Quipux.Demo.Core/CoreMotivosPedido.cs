﻿using SDI.API.Servicios.Base.Utilidades;
using SDI.API.Servicios.DTO;
using SDI.MotivosPedido.Repo;
using System;
using System.Collections.Generic;

using System.Reflection;

namespace SDI.MotivosPedido.Core
{
    /// <summary>
    /// Core GAP061 para Interfaz para sincronizar desde SAP ERP hacia el Consolidador de Pedidos los Motivos de Pedidos de SD que corresponden a Motivos de Devolución, Motivos de Cambio Mano a Mano, Motivos de Rechazo y Motivos de Reposiciones.
    /// </summary>
    public class CoreMotivosPedido
    {
        private List<ConsultaSpDTO> _consultas;
        private readonly RepoMotivosPedido _repoMotivosPedidos;

        public CoreMotivosPedido()
        {
            _repoMotivosPedidos = new RepoMotivosPedido();
            _consultas = CoreUtilidades.LoadJsonConsultaSP();
        }

        /// <summary>
        /// Realiza validaciones del negocio para el servicio que trae todos los registros
        /// </summary>
        /// <returns>Todos los registros de MotivosPedido</returns>
        public List<MotivoPedidoDTO> Consultar()
        {
            try
            {
                List<MotivoPedidoDTO> response = new List<MotivoPedidoDTO>();
                var consulta = _consultas.Find(x => x.Codigo == CodigosSp.GAP061);
                if (consulta == null || string.IsNullOrWhiteSpace(consulta.Ejecucion))
                {
                    //ToDo: Eliminar los datos "por defecto" cuando se conecte al SP
                    response.Add(new MotivoPedidoDTO());
                    response.Add(new MotivoPedidoDTO());
                    //throw new SDIException("");
                }
                else
                {
                    var operaciones = _repoMotivosPedidos.Consultar(consulta.Ejecucion);
                    response = Mapping.Mapper.Map<List<MotivoPedidoDTO>>(operaciones);
                }
               

                return response;
            }
            catch (SDIException appEx)
            {
                throw appEx;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        /// <summary>
        /// Realiza validaciones del negocio para el servicio que trae un registro especifico segun ID
        /// </summary>
        /// <param name="id">ID del registro a obtener</param>
        /// <returns>Registro de MotivosPedido</returns>
        public MotivoPedidoDTO ConsultarPorId(string id)
        {
            try
            {
                MethodBase metodo = MethodBase.GetCurrentMethod();
                MotivoPedidoDTO respuesta;
                var guid = Guid.Parse(id);
                var consulta = _consultas.Find(x => x.Codigo == CodigosSp.GAP060_OperacionesPorId);
                if (consulta == null || string.IsNullOrWhiteSpace(consulta.Ejecucion))
                {
                    //ToDo: Eliminar los datos "por defecto" cuando se conecte al SP
                    respuesta = new MotivoPedidoDTO();
                    //throw new SDIException((int)Errores.GAP060_Utilidades, metodo);
                }
                else
                {
                    //ToDo: Cuando se cree un SP dejar la consulta mediante este objeto a BD 
                    var operacion = _repoMotivosPedidos.ConsultarPorId(consulta.Ejecucion, guid);

                    //var operacion = _repoMotivosPedidos.ConsultarPorId(guid);
                    respuesta = Mapping.Mapper.Map<MotivoPedidoDTO>(operacion);
                }
                return respuesta;
            }
            catch (SDIException appEx)
            {
                throw appEx.Codigo > 0 ? appEx : appEx.ObtenerPorCodigo((int)Errores.Codigo_BaseDatos);
            }
            catch (Exception exc)
            {
                throw new SDIException(exc, (int)Errores.Codigo_ConsultaCore);
            }
        }


    }
}

