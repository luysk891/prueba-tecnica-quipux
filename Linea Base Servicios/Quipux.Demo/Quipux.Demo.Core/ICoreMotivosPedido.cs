﻿using SDI.API.Servicios.Base.Interfaces.ICore;
using SDI.API.Servicios.DTO;

namespace SDI.GAP061.Core
{
    public interface ICoreMotivosPedido: ICoreBase<MotivoPedidoDTO>
    {

    }
}
