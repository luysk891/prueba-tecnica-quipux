﻿using SDI.API.Servicios.Base.Repo;
using SDI.API.Servicios.Dominio;

namespace SDI.MotivosPedido.Repo
{
    public class RepoMotivosPedido: BaseRepositorio<MotivoPedido>, IRepoMotivosPedido
    {

    }
}
