﻿using SDI.API.Servicios.Base.Interfaces.IRepoBase;
using SDI.API.Servicios.Dominio;
namespace SDI.MotivosPedido.Repo
{
    public interface IRepoMotivosPedido: IRepoBase<MotivoPedido>
    {

    }
}
