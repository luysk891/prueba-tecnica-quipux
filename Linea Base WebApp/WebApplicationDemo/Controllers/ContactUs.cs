﻿using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplicationDemo.Controllers
{
	public class ContactUs : Controller
	{
		// GET: /<controller>/
		public IActionResult Index()
		{
			return View();
		}
		public IActionResult Welcome(string name, int numTimes = 1)
		{
			ViewData["Message"] = "Hello " + name;
			ViewData["NumTimes"] = numTimes;

			return View();
		}
	}
}
