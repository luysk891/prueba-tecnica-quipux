﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcMovie.Models
{
	public class Movie
	{
		public int Id { get; set; }
		[Display(Name = "Titulo")]
		[StringLength(60, MinimumLength = 3), Required]
		public string Title { get; set; }
		[DataType(DataType.Date)]
		[Display(Name = "Fecha de Estreno")]
		public DateTime ReleaseDate { get; set; }
		[RegularExpression(@"^[A-Z]+[a-zA-Z""'\s-]*$"), Required, StringLength(30)]
		[Display(Name = "Genero")]
		public string Genre { get; set; }
		[Display(Name = "Precio")]
		[Column(TypeName = "decimal(18, 2)")]
		public decimal Price { get; set; }
		[Display(Name = "Calificación")]
		[RegularExpression(@"^[A-Z]+[a-zA-Z0-9""'\s-]*$"), StringLength(5)]
		public String Rating { get; set; }
	}
}