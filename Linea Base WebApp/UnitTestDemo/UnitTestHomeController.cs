using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApplicationDemo.Controllers;

namespace UnitTestDemo
{
	[TestClass]
	public class UnitTestHomeController
	{
		[TestMethod]
		public void TestMethodGetMaxPrice()
		{
			HomeController homeController = new HomeController();

			int maxprice = homeController.GetMaxPrice();

			Assert.IsTrue(maxprice >= 1 && maxprice <= 255);

		}
		[TestMethod]
		public void TestMethodHomePrivacy()
		{
			HomeController homeController = new HomeController();
			IActionResult result = homeController.Privacy();
			Assert.IsInstanceOfType(result, typeof(ViewResult));
		}
		[TestMethod]
		public void TestMethodHomeIndex()
		{
			HomeController homeController = new HomeController();
			IActionResult result = homeController.Index();
			Assert.IsInstanceOfType(result, typeof(ViewResult));
		}
	}
}
